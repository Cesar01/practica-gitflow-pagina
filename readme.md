# Practica de Gitflow (contador)

Un contador básico elaborado para la practica de "Gitflow"

## Instalación

Tener **Node** instalado para poder ejecutar **npm**.

Ejecutar para instalar dependencias:

`npm istall`

## Levantar el servidor de desarrollo

`npm dev`

## Visualizar el proyecto

El proyecto se levanta en el puerto `8080`

[http://localhost:8080/](http://localhost:8080/)
