const COUNTER_KEY = "counter";
const INITIAL_COUNTER_VALUE = 0;

export const getCounterLocalStorage = async () => {
  if (!localStorage.getItem(COUNTER_KEY)) {
    localStorage.setItem(COUNTER_KEY, INITIAL_COUNTER_VALUE);
    console.log("sin localstorage");
    return localStorage.getItem(COUNTER_KEY);
  }

  return localStorage.getItem(COUNTER_KEY);
};

export const incCounterLS = () => {
  let counter = parseInt(localStorage.getItem(COUNTER_KEY)) + 1;
  localStorage.setItem(COUNTER_KEY, counter);
  return counter;
};

export const decCounterLS = () => {
  let counter = parseInt(localStorage.getItem(COUNTER_KEY)) - 1;
  localStorage.setItem(COUNTER_KEY, counter);
  return counter;
};

export const resetCounterLS = () => {
  localStorage.setItem(COUNTER_KEY, 0);
  return 0;
};
