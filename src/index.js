import {
  decCounterLS,
  getCounterLocalStorage,
  incCounterLS,
  resetCounterLS,
} from "./utils/counter.localstorage.js";

const $ = document;
let counter = await getCounterLocalStorage();
const $COUNTER = $.getElementById("COUNTER_DOM");
const $incButton = $.getElementById("incButton");
const $decButton = $.getElementById("decButton");
const $resetBUtton = $.getElementById("resetButton");
$COUNTER.innerHTML = counter;

/* Eventos para incrementear o decrementar el contador */
$incButton.addEventListener("click", async () => {
  $COUNTER.innerHTML = await incCounterLS();
});

$decButton.addEventListener("click", async () => {
  if (counter > 0) {
    $COUNTER.innerHTML = await decCounterLS();
  }
});

$resetBUtton.addEventListener("click", async () => {
  $COUNTER.innerText = await resetCounterLS();
});
